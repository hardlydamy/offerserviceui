import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPersonneComponent } from './components/add-personne/add-personne.component';
import { PersonneDetailsComponent } from './components/personne-details/personne-details.component';
import { PersonnesListComponent } from './components/personnes-list/personnes-list.component';

const routes: Routes = [
{path: '', redirectTo: 'personnes', pathMatch: 'full' },
{path: 'personnes', component: PersonnesListComponent },
{path: 'personnes/:id', component: PersonneDetailsComponent},
{path: 'add' , component: AddPersonneComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
