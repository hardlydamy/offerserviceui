import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonneService } from 'src/app/services/personne.service';
import { Personne } from  '../../model/personne';

@Component({
  selector: 'app-add-personne',
  templateUrl: './add-personne.component.html',
  styleUrls: ['./add-personne.component.css']
})
export class AddPersonneComponent implements OnInit {
  
  submitted = false;
  personne : Personne;
  
  constructor(
   private route : ActivatedRoute,
   private router: Router,
   private personneService: PersonneService) {
	this.personne = new Personne();
  }

  ngOnInit() {
  }

  savePersonne() {
    const data = {
      nom: this.personne.nom,
      prenom: this.personne.prenom,
	  age: this.personne.age,
    };
  
      this.personneService.create(this.personne)
      .subscribe(
        response => this.gotoPersonneList());

    this.submitted = true;
}

gotoPersonneList(){
	this.router.navigate(['/personnes']);
}
}