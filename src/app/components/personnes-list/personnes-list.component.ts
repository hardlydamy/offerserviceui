import { Component, OnInit } from '@angular/core';
import { Personne } from  '../../model/personne';
import { PersonneService } from '../../services/personne.service';

@Component({
  selector: 'app-personnes-list',
  templateUrl: './personnes-list.component.html',
  styleUrls: ['./personnes-list.component.css']
})
export class PersonnesListComponent implements OnInit {

  personnes : Personne[];

  constructor(private personneService : PersonneService) { }

  ngOnInit() {
	this.personneService.getAll().subscribe( data => {
		this.personnes = data;
	});
  }
}
