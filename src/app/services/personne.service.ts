import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Personne } from '../model/personne';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  private baseUrl : string;
  
  constructor(private http: HttpClient) 
  {
	this.baseUrl = 'http://localhost:8080/api/personnes';
  }
  
  public getAll(): Observable<Personne[]> {
	  return this.http.get<Personne[]>(this.baseUrl);
  }
  
  get(id) {
	  return this.http.get(`${this.baseUrl}/${id}`);
  }
  
  create(data) {
	  return this.http.post(this.baseUrl, data);
  }
  
  update(id, data) {
    return this.http.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll() {
    return this.http.delete(this.baseUrl);
  }
}
