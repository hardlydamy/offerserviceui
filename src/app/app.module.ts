import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddPersonneComponent } from './components/add-personne/add-personne.component';
import { PersonneDetailsComponent } from './components/personne-details/personne-details.component';
import { PersonnesListComponent } from './components/personnes-list/personnes-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPersonneComponent,
    PersonneDetailsComponent,
    PersonnesListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	FormsModule,
	HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
